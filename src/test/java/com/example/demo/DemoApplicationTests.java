package com.example.demo;


import static org.junit.jupiter.api.Assertions.assertEquals;

import java.lang.annotation.Annotation;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

class DemoApplicationTests {


	@Test
	void changeMethodName() {
		Object2 object2 = Object2.builder().field1("obj2.field1").field2("obj2.field2").field3("obj2.field3").build();
		Object1 object1 = Object1.builder().field1("obj1.field1").field2("obj1.field2").field3(3).object2(object2).build();

		String result = DemoApplication.changeMethodName(object1);
		assertEquals("Object1{object2=Object2{object1=null, field1=#masked#, field3=obj2.field3, field2=obj2.field2}, field1=obj1.field1, field3=3, field2=obj1.field2}", result);
		System.out.println(result);
	}

	@Test
	void changeMethodName_circularDependency() {
		Object2 object2 = Object2.builder().field1("obj2.field1").field2("obj2.field2").field3("obj2.field3").build();
		Object1 object1 = Object1.builder().field1("obj1.field1").field2("obj1.field2").field3(3).build();

		//Circular Dependency
		object1.setObject2(object2);
		object2.setObject1(object1);

		String result = DemoApplication.changeMethodName(object1);
		assertEquals("Object1{object2=Object2{field1=#masked#, field3=obj2.field3, field2=obj2.field2}, field1=obj1.field1, field3=3, field2=obj1.field2}", result);
		System.out.println(result);
	}


	@ParameterizedTest
	@CsvSource({
			"creditCard=1234567890123456,[0-9]{16},#masked#,creditCard=#masked#",
			"3,.*,'',''"
	})
	public void mask(Object value, String regex, String mask, String result) {
		String masked = DemoApplication.mask(value, new LogIgnore() {
			@Override
			public Class<? extends Annotation> annotationType() {
				return LogIgnore.class;
			}

			@Override
			public String regex() {
				return regex;
			}

			@Override
			public String mask() {
				return mask;
			}
		});
		assertEquals(masked, result);
	}

	@Getter
	@Setter
	@Builder
	static class Object1 {
		private String field1;
		@LogIgnore(regex = "creditCard=[0-9]{16}", mask = "")
		private String field2;
		private Integer field3;
		private Object2 object2;
	}

	@Getter
	@Setter
	@Builder
	static class Object2 {
		@LogIgnore
		private String field1;
		private String field2;
		private String field3;
		private Object1 object1;

	}
}
