package com.example.demo;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.validation.constraints.NotNull;

public class DemoApplication {
	public static void main(String[] args) {
	}

	public static String changeMethodName(Object object) {
		return changeMethodName(object, new HashSet<>());
	}

	private static String changeMethodName(Object value, @NotNull Set<String> set) {
		(set = set != null ? set : new HashSet<>()).add(Integer.toHexString(System.identityHashCode(value)));

		Map<String, String> fields = new HashMap<>();
		for (Field field : value.getClass().getDeclaredFields()) {
			boolean canAccess = field.canAccess(value);
			field.setAccessible(true);
			String fieldName = field.getName();
			try {
				Object fieldValue = field.get(value);

				if (fieldValue == null) {
					fields.put(fieldName, "null");
				} else if (field.getType().getDeclaringClass() != null) {
					if (set.contains(Integer.toHexString(System.identityHashCode(fieldValue)))) continue;
					fields.put(fieldName, changeMethodName(fieldValue, set));

				} else {
					String val = String.valueOf(fieldValue);
					for (LogIgnore logIgnore : field.getDeclaredAnnotationsByType(LogIgnore.class)) val = mask(val, logIgnore);
					fields.put(fieldName, val);
				}
			} catch (IllegalAccessException e) {
				fields.put(fieldName, "Error occurred when retrieve the value of filed: " + fieldName);
			} finally {
				field.setAccessible(canAccess);
			}
		}

		return value.getClass().getSimpleName() + fields.toString();
	}

	public static String mask(Object val, LogIgnore logIgnore) {
		return String.valueOf(val).replaceFirst(logIgnore.regex(), logIgnore.mask());
	}


}


