package com.example.demo;

import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
@Repeatable(LogIgnores.class)
public @interface LogIgnore {
	String regex() default ".*";
	String mask() default "#masked#";
}

